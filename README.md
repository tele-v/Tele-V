# Tele-V

## Install

### Software dependencies
*  [Git](https://git-scm.com/downloads)
*  [Python 3](https://www.python.org/downloads/)
*  [Hyper-V](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)

### Setup disk
* Create or use Disk "D:"
* Create folders "ISOs", "Scripts" and "VMs" in disk "D:"
* Do following steps (will be translated soon)

* Im Ordner “ISOs” 
    * Das Windows ISO unter https://www.microsoft.com/de-de/software-download/windows10ISO downloaden 
        * Auf Linux einfach machen
        * Auf Windows entweder mit 
            * Dem “angebotenen” Tool die Datei generieren und herunterladen oder 
            * Einem “UserAgent Switcher”-Browseraddon ein Linux simulieren und herunterladen
        * Danach den ISO-Filename anpassen auf obigen Namen 
    * Das Zorin ISO unter https://zorinos.com/download/15/core/64 downloaden

### Clone repo and config it

*  Execute in PowerShell or CMD in the folder "d:\Scripts"
`git clone https://gitlab.com/tele-v/Tele-V`
*  Create file "token.txt" and fill telegram bot token
*  Do steps describen in VMMgmt\VMs.json_template

### Start Bot
*  Start powershell as Admin
*  Execute `cd D:\Scripts\Tele-V\;python tele-v.py`
