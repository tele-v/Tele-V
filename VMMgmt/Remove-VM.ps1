<#
Author: GKE
.SYNOPSIS
Script to Remove a VM
.EXAMPLE
Inline: .\Remove-VM.ps1 -VMName "Test_01" -TelegramID "123456789" -Force $true
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [ArgumentCompleter(
        { 
            param( $Command, $Parameter, $WordToComplete, $CommandAst, $FakeBoundParams )
            ((Get-VM).Name) 
        }
    )]
    [ValidateScript( { $_ -in ((Get-VM).Name) })]
    [string]$VMName,
    [Parameter(Mandatory = $true)][Alias("TID")]
    [string]$TelegramID,
    [Parameter(Mandatory = $false)]
    [bool]$Force = $true
)


#region Import Log-Module
Import-Module ".\Log.psm1" -Force
if ($null -eq (Get-Module Log)) { return $false }
#endregion

$JSONPath = ".\VMs.json"
if (!(Test-Path "$JSONPath")) { Log -Message "JSON not present at $JSONPath" -IsError; return $false }
$Json = Get-Content "$JSONPath" | ConvertFrom-Json
$JsonLine = $json | Where-Object { $_.ID -eq "$TelegramID" }

if ( $JsonLine.VMs -notcontains $VMName) { 
    Log -Msg "VM $VMName could not be deleted: User $($JsonLine.Name) is not Authorized for VM $VMName" -IsError
    return $false
}

try {
    Remove-VM -Name $VMName -Force:$Force -ErrorAction Stop
    [array]($JsonLine.VMs) = [array]($JsonLine.VMs) | Where-Object { $_ -ne "$VMName" }
    Set-Content -Path $JSONPath -Value ($Json | ConvertTo-Json) -Force
    Remove-Item -Recurse "D:\VMs\$VMName" -Force
    Log -Msg "VM $VMName has been deleted and removed from $($JsonLine.Name) ($($JsonLine.ID))"
    return $true
}
catch {
    return "VM $VMName could not be deleted"
}