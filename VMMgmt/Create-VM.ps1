<#
Author: GKE
.SYNOPSIS
Script to Create a New VM
.EXAMPLE
Inline: .\Create-VM.ps1 -VMName "Test_01" -TelegramID "123456789"
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [string]$VMName,
    [Parameter(Mandatory = $true)][Alias("TID")]
    [string]$TelegramID,
    [Parameter(Mandatory = $true)][ValidateSet("Windows", "Zorin")]
    [string]$ISO,
    [Parameter(Mandatory = $false)]
    [int64]$Memory = 4GB,
    [Parameter(Mandatory = $false)]
    [int64]$CPU = 1,
    [Parameter(Mandatory = $false)]
    [string]$Switch = "Internal_Switch",
    [Parameter(Mandatory = $false)]
    [int]$Generation = 1,
    [Parameter(Mandatory = $false)]
    [string]$Path = "D:\VMs\",
    [Parameter(Mandatory = $false)]
    [string]$VHDPath = "D:\VMs\$VMName\VHD\Disk1.vhdx",
    [Parameter(Mandatory = $false)]
    [int64]$VHDSize = 16GB,
    [Parameter(Mandatory = $false)]
    [string]$Bootdevice = "VHD"
)

#region Import Log-Module
Import-Module ".\Log.psm1" -Force
if ($null -eq (Get-Module Log)) { return $false }
#endregion
$JSONPath = ".\VMs.json"
if (!(Test-Path "$JSONPath")) { Log -Message "JSON not present at $JSONPath" -IsError; return $false }
$Json = Get-Content "$JSONPath" | ConvertFrom-Json
$JsonLine = $Json | Where-Object { $_.ID -eq "$TelegramID" }

if ($ISO -eq "Zorin" ) {
    $ISOPath = "D:\ISOs\Zorin-OS-15-Core-64-bit-r1.iso"
    $OS = "Zorin"
}
else {
    $ISOPath = "D:\ISOs\en_windows_10_consumer_editions_version_1909_x64_dvd_be09950e.iso" 
    $OS = "Windows"
}

try {
    New-VM -Name $VMName -MemoryStartupBytes $Memory -BootDevice VHD -Path "$Path" -NewVHDPath "$VHDPath" -NewVHDSizeBytes $VHDSize -Generation $Generation -Switch $Switch -ErrorAction Stop | Out-Null
    [array]($JsonLine.VMs) += "$VMName"
    Set-Content -Path $JSONPath -Value ($Json | ConvertTo-Json) -Force
    Set-VMProcessor -VMName $VMName -Count $CPU
    Set-VMDvdDrive -VMName $VMName -Path $ISOPath
    Set-VM -Name $VMName -Notes "$OS"
    Log -Message "Successfully created VM $VMName and assigned to $($JsonLine.Name) ($($JsonLine.ID))"
    return "$true"
}
catch {
    Log -Message "Could not create VM $VMName" -IsError
    return "$false"
}