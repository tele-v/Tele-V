<#
Author: GKE
.SYNOPSIS
Script to Start a VM
.EXAMPLE
Inline: .\Start-VM.ps1 -VMName "Test_01" -TelegramID "123456789"
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [ArgumentCompleter(
        { 
            param( $Command, $Parameter, $WordToComplete, $CommandAst, $FakeBoundParams )
            ((Get-VM).Name) 
        }
    )]
    [ValidateScript( { $_ -in ((Get-VM).Name) })]
    [string]$VMName,
    [Parameter(Mandatory = $true)][Alias("TID")]
    [string]$TelegramID
)

#region Import Log-Module
Import-Module ".\Log.psm1" -Force
if ($null -eq (Get-Module Log)) { return $false }
#endregion

$JSONPath = ".\VMs.json"
if (!(Test-Path "$JSONPath")) { Log -Message "JSON not present at $JSONPath" -IsError; return $false }
$Json = Get-Content "$JSONPath" | ConvertFrom-Json
$JsonLine = $json | Where-Object { $_.ID -eq "$TelegramID" }

$VMsOnUser = $JsonLine.VMs

if ( $VMsOnUser -notcontains $VMName ) { "User $($JsonLine.Name) is not Authorized for VM $VMName"; return $false }

try {
    Start-VM -Name $VMName -ErrorAction Stop
    Log -Message "Started VM $VMName"
    return $true
}
catch [Microsoft.HyperV.PowerShell.VirtualizationException] {
    Log -Message "Could not start VM $VMName`: Not enough RAM available" -IsError
    return $false
}
catch {
    Log -Message "Could not start VM $VMName" -IsError
    return $false
}