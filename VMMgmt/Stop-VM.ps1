<#
Author: GKE
.SYNOPSIS
Script to Stop a VM
.EXAMPLE
Inline: .\Stop-VM.ps1 -VMName "Test_01" -TelegramID "123456789"
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [ArgumentCompleter(
        { 
            param( $Command, $Parameter, $WordToComplete, $CommandAst, $FakeBoundParams )
            ((Get-VM).Name) 
        }
    )]
    [ValidateScript( { $_ -in ((Get-VM).Name) })]
    [string]$VMName,
    [Parameter(Mandatory = $true)][Alias("TID")]
    [string]$TelegramID
)

#region Import Log-Module
Import-Module ".\Log.psm1" -Force
if ($null -eq (Get-Module Log)) { return $false }
#endregion

$JSONPath = ".\VMs.json"
if (!(Test-Path "$JSONPath")) { Log -Message "JSON not present at $JSONPath" -IsError; return $false }
$Json = Get-Content "$JSONPath" | ConvertFrom-Json
$JsonLine = $json | Where-Object { $_.ID -eq "$TelegramID" }

$VMsOnUser = $JsonLine.VMs

if ( $VMsOnUser -notcontains $VMName ) { "User $($JsonLine.Name) is not Authorized for VM $VMName"; return $false }

try {
    Save-VM -Name $VMName
    Log -Message "Saved state on VM $VMName"
    Stop-VM -Name "$VMName" -Force -Confirm:$false -WarningAction Stop -ErrorAction Stop
    Log -Message "Stopped VM $VMName by User $($JsonLine.Name) ($($JsonLine.ID))"
    return $true
}
catch {
    Log -Message "Could not stop VM $VMName" -IsError
    return $false
}