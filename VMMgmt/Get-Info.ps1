<#
Author: GKE
.SYNOPSIS
Script to get information to a specified VM
.EXAMPLE
Inline: .\Get-Infos.ps1 -VMName "Test_01" -TelegramID "123456789"


#>
[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [ArgumentCompleter(
        { 
            param( $Command, $Parameter, $WordToComplete, $CommandAst, $FakeBoundParams )
            ((Get-VM).Name) 
        }
    )]
    [ValidateScript( { $_ -in ((Get-VM).Name) })]
    [string]$VMName,
    [Parameter(Mandatory = $true)][Alias("TID")]
    [string]$TelegramID
)

$JSONPath = ".\VMs.json"
if (!(Test-Path "$JSONPath")) { Log -Message "JSON not present at $JSONPath" -IsError; return $false }
$Json = Get-Content "$JSONPath" | ConvertFrom-Json
$JsonLine = $json | Where-Object { $_.ID -eq "$TelegramID" }

$VMsOnUser = $JsonLine.VMs

if ( $VMsOnUser -notcontains $VMName ) { "User $($JsonLine.Name) is not Authorized for VM $VMName"; return $false }

$Information = Get-VM $VMName | Select-Object -Property Name, 
    @{Name = 'State'; Expression = { "$($_.State)" } }, 
    @{Name = 'OS'; Expression = { "$($_.Notes)" } }, 
    @{Name = 'Processor'; Expression = { $s = ""; if ($_.ProcessorCount -ne 1) { $s = "s" }; "$($_.ProcessorCount) Core$s" } }, 
    @{Name = 'RAM'; Expression = { "$($_.MemoryStartup / 1GB) GB" } },
    @{Name = 'DiskSpace'; Expression = { "$((Get-VHD -VMId $_.VMId).Size / 1GB) GB" } }

$OutJson = $Information | ConvertTo-Json -Compress
return $OutJson