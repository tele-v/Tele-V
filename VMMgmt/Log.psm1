function Log{
    <#
    Author: GKE
    .SYNOPSIS
    Log Function for all scripts
    .EXAMPLE
    Import-Module .\Log.psm1 -Force
    #>

    param (
        [Alias("Msg")]
        [Parameter(Mandatory = $true)]
        [string]$Message,
        [Parameter(Mandatory = $false)]
        [switch]$IsError,
        [Parameter(Mandatory = $false)]
        [string]$LOG_Path = "D:\Scripts\Log.log"
    )

    if ($IsError) { $ErrString = " Error:" }
    else { $ErrString = "" }
    $Date = Get-Date -UFormat "%d.%m.%y %H:%M:%S"
    $Content = "[$Date]   $ErrString $Message"
    Add-Content $Content -Path $LOG_Path
}